import  React, { Component } from "react";
import { BrowserRouter, Route, Switch, NavLink} from 'react-router-dom'; 

/* IMPORTANDO COMPONENTES */
import ClientesComponent from './componets/clientes/clientesComponent';
import FormaClientesComponent from './componets/clientes/formClientesComponent';
import HomeComponet from './componets/home/homeComponent';
import ErrorComponet from './componets/error/errorComponent';

class Router extends Component{
    
        render(){
            let title = "Prueba Técnica Infotec";
        return(
            /*CONFIGURAR RUTAS DEL SITIO*/ 
            <BrowserRouter>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">                    
                    <NavLink to="/" className="navbar-brand"> { title }</NavLink>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <NavLink to="/" className="nav-link"> Incio</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/clientes" className="nav-link"> Clientes</NavLink>
                        </li>                                                                        
                    </ul>                    
                    </div>
                </nav>     
                <Switch>
                    <Route exact path="/" component={HomeComponet}></Route>
                    <Route exact path="/clientes" component={ClientesComponent}></Route>
                    <Route exact path="/clientes/form" component={FormaClientesComponent}></Route>                    
                    <Route exact path="/clientes/form/:id" component={FormaClientesComponent}></Route>                    

                    <Route component={ErrorComponet}></Route>      
                </Switch>
            </BrowserRouter>
        )
    }
}

export default Router;