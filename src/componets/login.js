
import { API_URL, SESSION_USER } from '../store/config';

export const login = (data) => {
	const body = {		
		username: data.username,
		password: data.password
	}
	const options = {
		headers: {
			'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
		},
		method: 'post',
		body: JSON.stringify(body)
	}

	return dispatch => {		
		return fetch(`${API_URL}/api/login`, options)
			.then(result => {
                console.log(result);
                SESSION_USER = sessionStorage.setItem('token', 'token1');
                window.location = "/clientes";
				/*if (result.status >= 200 && result.status < 300) {
					return result.json();
				} else {
					return result.json().then(Promise.reject.bind(Promise));
				}*/
			})
			.then(
				/*json => {
					let session = {
						...json,
						remember: data.remember ? true : false
					}
					return dispatch(validateSession(session))
				},
				error => dispatch(loginFail(error.message ? (error.errors ? `${error.message} - ${_.values(error.errors)}`:error.message):_.values(error)))*/
			);
	}
}

export const logout = () => {
	window.location = "/";
}
