import { Component } from 'react';
import { Link } from 'react-router-dom';
import { API_URL, STORAGE_URL } from '../../store/config';
import axios from 'axios';
import Moment from 'react-moment';
import 'moment/locale/es-mx';
import Swal from 'sweetalert2';
import { logout } from '../login';


class ClientesComponet extends Component{
    
    state = {
        clientes: [],
        status: null
    }    
    

    componentWillMount(){
        this.getClientes();
    }

    getClientes = () => {
        const session = sessionStorage.getItem('token');        
        if(  session === null ){
            console.log(session);
			logout();
			return false;
		}        

        const options = {
            headers: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization': 'Bearer '+session
            }
        }

        axios.get(`${API_URL}/api/clientes`,options).then((res) =>{
            console.log(res.data);    
            var datos = res.data.data
            this.setState({
                clientes: datos,
                status: 'success'
            })           
            console.log(this.state)
        });
    }

    deleteCliente = (id) =>{
        Swal.fire({
            icon: 'warning',
            title: '¿Realmente desea eliminar al usuario?',            
            showCancelButton: true,                        
            confirmButtonText: `Sí, eliminar`,
            cancelButtonText: `No, cancelar`,
          }).then((result) => {            
            if (result.isConfirmed) {
              Swal.fire('Saved!', '', 'success')
            } else {
              Swal.fire('Solicitud cancelada', '', 'info')
            }
          })
    } 

    

    render(){             
        

        return(
            <div className="header-componet">                
                <div className="card border-primary mb-3">
                    <div className="card-header">Clientes</div>
                    <div className="card-body text-primary">
                        <h5 className="card-title">Listado de clientes</h5>
                        <div className="my-2 text-left">
                            <Link className="btn btn-rounded btn-primary" to="/clientes/form">
                                Crear Cliente
                            </Link>
                        </div>
                        {
                            this.state.clientes.length === 0 ?
                            <div className="alert alert-info">
                                No hay registros en la base de datos!
                            </div> : ""
                        }
                        
                        <table className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>nombre</th>
                                <th>apellido</th>
                                <th>email</th>
                                <th>fecha</th>
                                <th>editar</th>
                                <th>eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.clientes.map((cliente, key) => {                                        
                                        return(
                                            <tr >
                                                <td>
                                                    {
                                                        cliente.foto !== null ? 
                                                        (
                                                            <img src={`${API_URL+STORAGE_URL+cliente.foto}`} style={{width: 65 + 'px', cursor: 'pointer'}}></img>
                                                        ) :
                                                        (
                                                            <img src={`${API_URL+STORAGE_URL+'no-usuario.png'}`} style={{width: 65 + 'px', cursor: 'pointer'}}></img>
                                                        )
                                                    }
                                                    
                                                </td>
                                                <td>{ cliente.nombre }</td>
                                                <td>{ cliente.apellido }</td>
                                                <td>{ cliente.email }</td>
                                                <td>
                                                <Moment format='DD/mm/YYYY'>{ cliente.created_at}</Moment>
                                                    </td>     
                                                <td>
                                                    <Link to={"/clientes/form/"+cliente.id}  className="btn btn-primary btn-sm">editar</Link>
                                                </td>
                                                <td>
                                                    <Link className="btn btn-danger btn-sm">eliminar</Link>
                                                </td>
                                            </tr>
                                        )                                        
                                    })  
                                }                                                             
                            </tbody>
                        </table>
                                                                                            
                    </div>
                    </div>

            </div>
        )
    }
}

export default ClientesComponet;

