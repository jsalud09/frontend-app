import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { API_URL, STORAGE_URL } from '../../store/config';
import axios from 'axios';



class FormClientesComponet extends Component {

    URL = API_URL;
    id = undefined;

    state = {
        cliente: {},
        status: null
    }

    nombreRef = React.createRef();
    apellidoRef = React.createRef();
    emailRef = React.createRef();


    componentWillMount() {
        this.getCliente();
    }

    getCliente = () => {
        this.id = this.props.match.params.id;
        console.log(this.id);
        if (this.id !== undefined) {
            axios.get(this.URL + '/clientes/' + this.id).then((res) => {
                console.log(res);
                this.setState({
                    cliente: res.data,
                    status: 'success'
                })
                console.log(this.state)
            });
        }
    }

    enviarFormulario = (e) => {
        e.preventDefault();
        this.changeState();
        const headers = {
            'Content-Type': 'application/json'
        };
        //Enviar formulario para guardar
        axios.post(this.URL + '/clientes', this.state.cliente, this.headers)
            .then(result => {
                console.log(result)
            });
    }

    changeState = () => {
        this.setState({
            cliente: {
                nombre: this.nombreRef.current.value,
                apellido: this.apellidoRef.current.value,
                email: this.emailRef.current.value
            }
        });
        console.log(this.state);
    }


    render() {
        let titulo = '';
        if (this.id === undefined) {
            titulo = "Crear Cliente";
        } else {
            titulo = "Editar Cliente";
        }
        var cliente = this.state.cliente;
        return (
            <div id="formulario" className="container">
                <div className="row row-cols-2">
                    <div className="col">
                        <div className="card bg-dark text-white">
                            <div className="card-header">{titulo}</div>
                            <div className="card-body">
                                <form onSubmit={this.enviarFormulario} >
                                    <div className="col">
                                        <div className="form-group row">
                                            <label for="nombre" className="col-sm-2">Nombre</label>
                                            <div className="col-sm-10">
                                                <input type="text" className="form-control" name="nombre" ref={this.nombreRef} onChange={this.changeState} />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label for="apellido" className="col-sm-2">Apellido</label>
                                            <div className="col-sm-10">
                                                <input type="text" className="form-control" name="apellido" ref={this.apellidoRef} onChange={this.changeState} />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label for="email" className="col-sm-2">Email</label>
                                            <div className="col-sm-10">
                                                <input type="email" className="form-control" name="email" ref={this.emailRef} onChange={this.changeState} />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label htmlFor="foto" className="col-sm-2">Seleccionar foto:</label>
                                            <div className="col-sm-10">
                                                <input type="file" name="file" className="form-control"></input>
                                            </div>
                                        </div>


                                        <div className="form-group row">
                                            <div className="col-sm-12 text-right">
                                                <button className="btn btn-primary" type="submit" >Crear</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {
                        cliente.foto &&
                        <div className="col">
                            <div className="card bg-dark text-white">
                                <div className="card-body">
                                    <img src={`${API_URL + STORAGE_URL + cliente.foto}`} className="img-fluid" alt="Responsive image"></img>
                                </div>
                            </div>
                        </div>
                    }
                </div>

            </div>
        )
    }
}

export default FormClientesComponet;