import { Component } from 'react';
import { API_URL, SESSION_USER } from '../../store/config';
import axios from 'axios';
import Swal from 'sweetalert2';
import './homeComponent.css';



class HomeComponet extends Component {

    state = {
        data:{
            username: '',
            password: ''
        }
    }

    handleChange = async (e) =>{
        await this.setState({
            data:{
                ...this.state.data,
                [e.target.name]: e.target.value            
            }
        })
        console.log(this.state.data);
    }

    iniciarSesion = async (e) =>{
        e.preventDefault();
        const body = {		
            email: this.state.data.username,
            password: this.state.data.password
        }
        const options = {
            headers: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },                        
            email: this.state.data.username,
            password: this.state.data.password            
        }
        var session_user = '';    
        axios.post(`${API_URL}/api/login`, options)
            .then(result => {                
                var response = result.data;
                if(response.status === 'success'){
                    sessionStorage.setItem('token',response.token);                
                    Swal.fire(
                        'Inicio de sesión',
                        response.message,
                        'success'
                    ).then((e)=>{
                        window.location = "/clientes"                
                    });                    
                }else{
                    Swal.fire(
                        'Inicio de sesión',
                        response.message,
                        'error'
                    )
                }                                
            })               
    }

    render() {

        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-6">
                        <div className="card">
                            <div className="card-header">
                                Iniciar sesión
                            </div>
                            <div className="card-body">
                                <form onSubmit={this.iniciarSesion}>
                                <input className="form-control" name="username" type="email" onChange={this.handleChange} />
                                <input className="form-control" name="password" type="password" onChange={this.handleChange} />
                                <input className="btn btn-primary" type="submit" value="Iniciar sesión" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default HomeComponet;

