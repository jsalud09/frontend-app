import React, { Component } from 'react';


const footerComponet = () => {
        let autor = {nombre: 'José Luis', apellido: 'Salud Jiménez'}

        return(
            <div className="header-componet">
                <footer className="footer bg-dark rounded-top text-center">
                    <div className="container py-2">
                        <p className="text-white my-2">
                        &copy; { autor.nombre + ' ' + autor.apellido}
                        </p>
                    </div>
                </footer>
            </div>
        )

}

export default footerComponet;

