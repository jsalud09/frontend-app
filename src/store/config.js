/* API */
let auth_url_config = '';
let storage_url = '';
let session_user = '';
console.log(window.location.hostname);
if (window.location.hostname === 'localhost') {
    auth_url_config = 'http://localhost:8000';    
    storage_url = '/storage/images/';    
}

export const API_URL = auth_url_config;
export const STORAGE_URL = storage_url;
export const SESSION_USER = session_user;
  