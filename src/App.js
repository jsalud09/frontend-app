import './App.css';


//Componetes creados
import FooterComponet from './componets/footer/footerComponent';

//Componente de ruta
import Rutas from './Router';

function App() {
  return (
    <div className="App">      
      <Rutas />                  
      <FooterComponet />    
    </div>
  );
}

export default App;
